# Controller
### Responsabilidade
Controller é a conexão principal entre presenter e a view. Também faz o papel da View quando não separado.

### Observação
* O Controller podem utilizar outros controllers para cumprir suas responsabilidades.

### Diagrama de interação
![controllerdiagram](controllerdiagram.png)

### Tipos comuns de Controllers
Aqui estão alguns dos [tipos comuns de controllers](CommonControllerTypes.md) que você pode usar.

### Exemplo de código

```swift
protocol ExampleViewControllerProtocol {
    func showView(with data: Any)
    func showRetryView(with error: PrintableError)
}

final class ExampleViewController: UIViewController {
    
    private(set) weak var navigation: ProductSearchNavigationListener?
    private(set) var presenter: ProductSearchPresenterProtocol

    private(set) lazy var contentView: UIView = {
        let view = UIView()
        view.accessibilityIdentifier = "exampleViewControllerContentView"
        return view
    }()

     private(set) lazy var exampleView: ExampleView = {
        let view = ExampleView()
        view.accessibilityIdentifier = "exampleViewControllerExampleView"
        exampleView.delegate = self
        return view
    }()

    private(set) lazy var exampleButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(performLogin(_:)), for: .touchUpInside)
        return button
    }()

    init(navigation: ExampleNavigationListener?,
         presenter: ExamplePresenterProtocol = ExamplePresenter()) {
        self.navigation = navigation
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        self.presenter.view = self
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.fetchData()
    }
}

extension ExampleViewController:  ExampleViewControllerProtocol {

    func showView(with data: Any) {
        exampleView.updateView(with: data)
    }

    func showRetryView(with error: PrintableError) {
        exampleView.showRetryView(with: error)
    }
}

extension ExampleViewController: CodeView {
    func buildViewHierarchy() {
        contentView.addSubview(exampleView, exampleButton)
        view.addSubview(contentView)
    }

    func setupConstraints() {
        contentView.anchor(
            top: view.topAnchor,
            leading: view.leadingAnchor,
            bottom: view.bottomAnchor,
            trailing: view.trailingAnchor
        )

        exampleView.anchor(
            top: contentView.safeAreaLayoutGuide.topAnchor,
            leading: contentView.leadingAnchor,
            bottom: contentView.bottomAnchor,
            trailing: contentView.trailingAnchor
        )

        exampleButton.anchor(
            leading: contentView.leadingAnchor,
            bottom: contentView.bottomAnchor,
            trailing: contentView.trailingAnchor
        )
    }

    func setupAdditionalConfiguration() {
        view.backgroundColor = Config.color.whiteColor
    }
}
```
