# Presenter
### Responsabilidade
Recebe ações a partir da view, faz as requisições necessárias para a camada de modelo para lidar com as ações apropriadamente, e formata os dados para serem apresentados na view.
### Observações
* Os Presenters definem o ponto único de configuração para propriedades exibidas.
* A maioria das propriedades pode ser facilmente mapeada a partir de propriedades semelhantes no Modelo.
* Os Presenters contêm a lógica para transformar suas próprias propriedades em versões exibidas (por exemplo, um objeto de data em texto localizável e legível por humanos).
* A lógica de interação (por exemplo, evento de toque do usuário) deve ser tratada pela própria View, não pelo Presenter.

### Diagrama de interação
![controllerdiagram](controllerdiagram.png)

### Exemplo de código
```swift
protocol ExamplePresenterProtocol: AnyObject {
    var view: ExampleViewControllerProtocol? { get set }
    func fetchRemoteData()
    func fetchLocalData()
}

final class ExamplePresenter: ExamplePresenterProtocol {
    var view: ExampleViewControllerProtocol?

    private let service: ExampleServiceProtocol
    private let storage: ExampleStorageProtocol

    init(service: ExampleServiceProtocol = ExampleService(),
         storage: ExampleStorageProtocol = ExampleStorage()) {
        self.service = service
        self.storage = storage
    }

    func fetchRemoteData() {

    }

    func fetchLocalData() {

    }
}
```
