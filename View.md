# View
### Responsabilidade
As views são responsáveis por processar o conteúdo e manipular a interação do usuário com esse conteúdo.

### Consideracoes chave
* As views são responsáveis ​​pelo estilo e layout dos componentes da interface do usuário.
* As views são uma representação visual de seus modelos.
* As views personalizadas compostas de outras views definem uma interface para configurar as propriedades de exibição de seu conteúdo por meio de seu [modelo de visualização] (https://github.com/Lickability/swift-style-guide/blob/master/ViewModel.md) .
* Para algumas views, a interação do usuário é comunicada aos controladores por meio de delegates ou closures.
* Para views herdadas do `UIControl` (como` UISwitch`, `UIButton`,` UISlider`, etc ...), a interação do usuário é comunicada através de uma [target-action] (https://developer.apple. com / library / archive / documentation / General / Conceitual / Devpedia-CocoaApp / TargetAction.html # // apple_ref / doc / uid / TP40009071-CH3) para notificar seu aplicativo quando ocorrer uma interação.
* O mecanismo [target-action] (https://developer.apple.com/library/archive/documentation/General/Conceptual/Devpedia-CocoaApp/TargetAction.html#//apple_ref/doc/uid/TP40009071-CH3) pode seja combinado com [delegation] (https://developer.apple.com/library/archive/documentation/General/Conceptual/DevPedia-CocoaCore/Delegation.html) ou [closures] (https://docs.swift.org/ swift-book / LanguageGuide / Closures.html) para delegar a responsabilidade de manipular a ação para outro controlador. Veja abaixo um exemplo de código.

### Interaction Diagram  
![controllerdiagram](controllerdiagram.png)

### Exemplo de código

```swift
public protocol ExampleViewDelegate: AnyObject {
    func didSelect(suggestion: String)
    func didTouchRetryButton()
}

final class ExampleView: UIView {
    weak var delegate: ExampleViewDelegate?

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(ExampleViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.accessibilityIdentifier = "exampleViewTableView"
        tableView.isHidden = true
        return tableView
    }()

    private lazy var headerView: HeaderView = {
        let view = HeaderView()
        view.accessibilityIdentifier = "exampleViewHeaderView"
        /// Closures
        view.didSelectClearFilters = { [weak self] in
            self?.delegate?.didTapCleanOrder()
        }

        view.didSelectFilterAtIndex = { [weak self] index in
            self?.handleFilterSelect(at: index)
        }
        return view
    }()

    private lazy var emptyView: EmptyStateView = {
        let view = EmptyStateView(frame: .zero, respondsToKeyboard: false)
        let image = ResourceKit.Asset.DefaultAssets.icEmptySearchProducts.image
        let title = L10n.productSearch
        let message = L10n.beginProductSearchText
        view.setup(image: image, title: title, message: message)
        view.accessibilityIdentifier = "exampleViewEmptyView"
        return view
    }()

    private lazy var retryView: RetryView = {
        let view = RetryView()
        view.isHidden = true
        /// Target Action
        view.addButtonAction(delegate, buttonAction: #selector(handleRetryViewButtonAction(_:)))
        view.accessibilityIdentifier = "exampleViewRetryView"
        return view
    }()

    private var data: [String] = []

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        setupView()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func updateView(with newData: [String]) {
        data = newData
        tableView.isHidden = false
        emptyView.isHidden = true
        retryView.isHidden = true

        tableView.reloadData()
    }

    func showEmptyView() {
        emptyView.isHidden = false
        tableView.isHidden = true
        retryView.isHidden = true
    }

    func showRetryView(with error: PrintableError) {
        tableView.isHidden = true
        emptyView.isHidden = true
        retryView.isHidden = false
        retryView.setup(with: error)
    }

    @objc
    private func handleRetryViewButtonAction(_: UIButton) {
        delegate?.didTouchRetryButton()
    }
}

extension ExampleView: CodeView {
    func buildViewHierarchy() {
        [tableView, emptyView, retryView].forEach(addSubview)
    }

    func setupConstraints() {
        [tableView, emptyView, retryView].forEach {
            $0.anchor(
                top: safeAreaLayoutGuide.topAnchor,
                leading: leadingAnchor,
                bottom: bottomAnchor,
                trailing: trailingAnchor,
                insets: .zero
            )
        }
    }

    func setupAdditionalConfiguration() {
        backgroundColor = Config.color.whiteColor
    }
}

extension ExampleView: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return suggestions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ExampleViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.setup(suggestion: suggestions[indexPath.row])
        return cell
    }
}

extension ExampleView: UITableViewDelegate {
    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Delegation
        delegate?.didSelect(suggestion: suggestions[indexPath.row])
    }
}

```
