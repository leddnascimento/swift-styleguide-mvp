# Diagrama de arquitetura

![architecturediagram](architecturediagram.png)

### [Model](Model.md)
Entidades que representam os dados da aplicação.

### [View](View.md)
Responsável por renderizar a tela, receber ações do usuário e informar o presenter sobre elas.

### [Controller](Controller.md)
Controller é a conexão principal entre presenter e a view. Também faz o papel da View quando não separado.

### [Presenter](Presenter.md)
Recebe ações a partir da view, faz as requisições necessárias para a camada de modelo para lidar com as ações apropriadamente, e formata os dados para serem apresentados na view.

### [Coordinator](Coordinator.md)
Responsável pelo fluxo de navegação da aplicação. Também é sua responsabilidade iniciar fluxos quando necessário.

### [Service](Service.md)
Os elementos dessa camada são responsáveis por coisas como requisição de rede, persistência, parser de informações e assim por diante.