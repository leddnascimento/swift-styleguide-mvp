# Modelo
### Responsabilidade
Modelos são a estrutura de dados dinâmica do aplicativo, independente da interface do usuário. Eles gerenciam diretamente os dados e a lógica comercial do aplicativo.

### Observações
* Os modelos podem estruturar seus dados de forma confiável e prepará-los com base nas instruções do controlador.
* Eles não são responsáveis por recuperar dados das camadas de persistência ou rede.
* A mutabilidade nos modelos deve ser evitada, optando por recriar o modelo quando as informações forem alteradas. Modelos mutáveis podem criar condições de corrida quando os dados estão sendo gravados e lidos simultaneamente em vários segmentos ou filas.
* Para tornar mais flexível os testes que usam o modelo, é recomendado  ter um contrato e usá-lo para garantir um baixo acoplamento. 
  
### Diagrama de interação
![modeldiagram](modeldiagram.png)

### Exemplo de código

```swift
protocol ProductProtocol {
    var serialNumber: String { get set }
    var name: String { get set }
    var description: String? { get set }
    var price: Int { get set }
    var category: Product.Category { get set }
    var isEdible: Bool { get set }
}

struct Product: ProductProtocol {
    
    enum Category {
        case food
        case beverage
        case merchandise
    }
    
    var serialNumber: String
    var name: String
    var description: String?
    var price: Int
    var category: Category

    var isEdible: Bool {
        get {
            switch category {
            case .food:
                return true
            case .beverage, .merchandise:
                return false
            }
        }
    }
}
```
